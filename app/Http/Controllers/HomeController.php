<?php

namespace App\Http\Controllers;

use App\Drink;
use App\Record;
use App\Service\Calculator;
use Illuminate\Http\Request;
use View;
use Response;
use Validator;

class HomeController extends Controller
{
    public function index() {
        return view('home');
    }
    
    public function blog() {
        return view('blog/blog');
    }

    public function getPost() {
        return view('blog/blog_post');
    }

    public function sitemap() {
        return response()->view('sitemap', compact('pages'))->header('Content-Type', 'text/xml');
    }

    public function getSharedPage() {
        return view('share.shared');
    }

    public function getShare() {
        return view('share.share');
    }

    public function calculate(Request $request) {
        $request->validate([
            'weight' => 'required|integer|between:1,250',
            'gender' => 'required',
            'values.*.amount' => 'nullable|integer|min:0|max:2000',
            'values.*.alcohol' => 'nullable|numeric|min:0.1|max:100',
            'hours' => 'nullable|integer|min:0'
        ]);
        $calculator = new Calculator();
        $calculator->startRecord();
        $calculator->processWeight($request->weight, $request->gender);
        $calculator->setHours($request->hours);

        foreach($request->values as $drink) {
            $calculator->processDrink($drink);
        }
        $result = $calculator->calculateAlcohol();
        $calculator->updateRecord();

        return response()->json($result);
    }

    public function search($keyword) {
        $drinks = Drink::search($keyword)->get();
        $unique = $drinks->unique(function ($item) {
            return $item['name'].$item['alcohol'].$item['volume'];
        });
        $uniqueDrinks = $unique->values();
        $uniqueDrinks->all();
        return response()->json($uniqueDrinks);
    }
}
