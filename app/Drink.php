<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    use Searchable;

    public $asYouType = true;

	protected $fillable = ['foreign_id'];

	protected $hidden = ["created_at", "updated_at", "id"];

    public function searchableAs()
    {
        return 'drinks_index';
    }
}
