<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Drink;

class import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports drinks from systembolaget';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xml = \XmlParser::load('https://www.systembolaget.se/api/assortment/products/xml');

        $articles = $xml->parse([
            'article' => ['uses' => 'artikel[Artikelid,Namn,Volymiml,Alkoholhalt]']
        ]);
        foreach($articles["article"] as $article)
        {
            $drink = Drink::firstOrNew(['foreign_id' => $article["Artikelid"]]);
            $drink->foreign_id = $article["Artikelid"];
            $volume = $article["Volymiml"] / 10;
            $drink->volume= $volume;
            $drink->name = $article["Namn"];
            $alcohol = str_replace("%",'',$article["Alkoholhalt"]);
            $drink->alcohol = $alcohol;
            $drink->save();
        }
    }
}
