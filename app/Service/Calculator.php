<?php
namespace App\Service;
use App\Record;
use App\RecordDrink;

/**
 * Created by PhpStorm.
 * User: Jonte
 * Date: 2018-01-08
 * Time: 12:27
 */
class Calculator {
    private $amountOfDrinks = NULL;
    private $record = NULL;
    private $totalFortyPercent = NULL;
    private $weight = NULL;
    private $gender = NULL;
    private $hours = 0;
    private $recordId = NULL;
    private $permilleToSave = NULL;
    private $hardCodedValues = Array('beer','booze', 'wine', 'other');

    public function calculateAlcohol() {
        $returnArray = array();
        $gram = $this->totalFortyPercent * 3.3;
        $permille = $gram / $this->weight;
        $this->permilleToSave = round($permille, 2, PHP_ROUND_HALF_UP);

        while($permille > 0) { 
            if($this->hours == 0) {
                $this->permilleToSave = round($permille, 2, PHP_ROUND_HALF_UP);
            }
            $permille = $permille - 0.15;
            $this->hours++;
        }

        $returnArray['permille'] = $this->permilleToSave;
        $returnArray['hours'] = $this->hours;

        return $returnArray;
    }

    public function setHours($hours) {
        $this->hours = -$hours;
    }

    public function startRecord() {
        $this->record = new Record();
        $this->record ->save();
        $this->recordId = $this->record->id;
    }

    public function updateRecord() {
        $this->record->drinks = $this->amountOfDrinks;
        $this->record->permille = $this->permilleToSave;
        $this->record->hours = $this->hours;
        $this->record->save();
    }

    public function processWeight($weight, $gender) {
        if(strcmp($gender[0], 'male') !== 0) {
            $this->weight = $weight / 100 * 60;
            $this->gender = 'female';
        }
        else {
            $this->weight = $weight / 100 * 70;
            $this->gender = 'male';
        }
    }

    public function processDrink($drink) {
        if($drink["amount"] && $drink["alcohol"]) {
            $this->totalFortyPercent += $drink["alcohol"] * $drink["amount"] / 40;
            $this->amountOfDrinks ++;

            $recordDrink = new RecordDrink();
            $recordDrink->record_id = $this->recordId;
            $recordDrink->type = $drink["name"];
            $recordDrink->amount = $drink["amount"];
            $recordDrink->alcohol = $drink["alcohol"];

            if(!in_array($drink["name"], $this->hardCodedValues)) {
                $recordDrink->drink_id = $drink["foreign_id"];
            }
            $recordDrink->save();/*?? throw new \Exception("Invalid amount  of alcohol")*/
        }
     }
}

//män kg * 70% = x av kroppsvikt
//kvinnor kg * 60% = x av kroppsvikt
//gram alkohol / kroppsvikt = promille
//0.15 promille / timme
//1%/1cl = 0.08g
//70x13=910 delat i 40=22.75 delat i 2=11,3 timmar
//(dryckens alkoholhalt i %)*(dryckens mängd i cl)/(önskad jämförelsefaktor 40%)
//ex 5,3(%) * 33(cl) / 100(%) = 1.749 100% alkohool