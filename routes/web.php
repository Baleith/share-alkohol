<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::post('/calculate', 'HomeController@calculate')->name('calculate');

Route::get('/search/{keyword}', 'HomeController@search');

Route::get('/blog', 'HomeController@blog');

Route::get('/get/post', 'HomeController@getPost');

Route::get('/sitemap','HomeController@sitemap');