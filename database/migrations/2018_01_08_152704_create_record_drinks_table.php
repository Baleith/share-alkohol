<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordDrinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_drinks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('record_id');
            $table->string('type');
            $table->decimal('alcohol', 5, 2);
            $table->string('amount');
            $table->integer('drink_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_drinks');
    }
}
