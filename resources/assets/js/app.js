
//require('./bootstrap');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 
//Vue.component('example-component', require('./components/Example.vue'));
var ComponentA = require('./components/Example.vue');

const app = new Vue({
	el: '#wrapper',
	components: {
		'component-a': ComponentA
	}
});

// $(function(){
//     "use strict";
//     var error_container = $('.error-container');
//     var result_container = $('.result-container');
//     var weight = $('#weight');
//     var divider_forms = $('.divider-forms');
//     var button_search_container = $('.button-search-container');
//     var btn_next = $("#btn_next");
//     var btn_prev = $("#btn_prev");
//     var listing_table = document.getElementById("listingTable");
//     var current_page = 1;
//     var records_per_page = 5;
//     var search_result_selected = document.getElementById("search-result-selected");
//     var objJson = [];

//     $('#calculate-form').on('submit',function(e){
//         $.ajaxSetup({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             }
//         });
//         e.preventDefault(e);

//         $.ajax({
//             type:"POST",
//             url:'/calculate',
//             data:$(this).serialize(),
//             dataType: 'json',
//             success: function(data) {
//                 if(data.result.hours < 0 ) {
//                     var hours = Math.abs(data.result.hours);
//                     $('.alert-success').html('' +
//                         'Du har ca <span id="result-permille">'+data.result.permille+'</span> promille i blodet. <br>' +
//                         ' Enligt ungefärlig beräkning så förbrände du den sista alkoholen för <span id="result-hours">'+hours+'</span> timmar sedan.'
//                     );
//                 }
//                 else {
//                     $('.alert-success').html('' +
//                         'Du har ca <span id="result-permille">'+data.result.permille+'</span> promille i blodet. <br>' +
//                         'Det kommer enligt ungefärlig beräkning ta <span id="result-hours">'+data.result.hours+'</span> timmar innan du är helt nykter.');
//                 }

//                 result_container.removeClass('no-display');
//                 error_container.addClass('no-display');
//             },
//             error: function(data){
//                 var error_list = $('#error-list');
//                 error_list.empty();
//                 $.each( data.responseJSON.errors, function( index, value ) {
//                     error_list.append('<li>' + value[0] + '</li>');
//                 });
//                 error_container.removeClass('no-display');
//                 result_container.addClass('no-display');
//             }
//         })
//     });
//     $('#calculate-form').on('keyup keypress', function(e) {
//         var keyCode = e.keyCode || e.which;
//         if (keyCode === 13) {
//             e.preventDefault();
//             return false;
//         }
//     });
//     $('.btn-decrease').on('click', function () {
//         var weight_val = parseInt(weight.val());
//         weight.val(weight_val - 1);
//     });
//     $('.btn-increase').on('click', function () {
//         var weight_val = parseInt(weight.val());
//         weight.val(weight_val + 1);
//     });
//     $('#beer').on('change', function (e) {
//         var beerType = $('#select-beer').val();
//         $('#beer_hidden').val(beerType * e.target.value);
//     });
//     $('#select-beer').on('change', function(e) {
//         if(e.target.value === '1') {
//             $('#beer').attr("placeholder", "cl");
//         }
//         else {
//             $('#beer').attr("placeholder", "Antal");
//         }
//     });
//     $('#reset').on('click', function () {
//         $('#listingTable').empty();
//         $('#search-result-selected').empty();
//         error_container.addClass('no-display');
//         result_container.addClass('no-display');
//         divider_forms.addClass('no-display');
//         button_search_container.addClass('no-display');
//     });
//     $('#search').on('click', function () {
//         $.ajaxSetup({
//             headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             }
//         });
//         let keyword = document.getElementById('search-keyword').value;
// console.log('diten')
//         $.ajax({
//             type:"get",
//             url:'/search/' + keyword,
//             dataType: 'json',
//             success: function(data) {
//                 if(jQuery.isEmptyObject(data)) {
//                     btn_prev.addClass('no-display');
//                     btn_next.addClass('no-display');
//                     listing_table.innerHTML = "Tyvärr hittar vi ingen produkt som matchar din sökning.";
//                 }
//                 else {
//                     current_page = 1;
//                     objJson = data;
//                     $('#listingTable').empty();
//                     changePage(1);
//                 }
//             },
//             error: function(data){
//                // console.log(data.error);
//             }
//         })
//     });

//     $('#search-keyword').keyup(function(e) {
//         clearTimeout($.data(this, 'timer'));
//         console.log('daten')
//         if (e.keyCode == 13)
//             search(true);
//         else
//             $(this).data('timer', setTimeout(search, 500));
//     });
//     function search(force) {
//         var keyword = $("#search-keyword").val();
//         if (!force && keyword.length < 3) return; //wasn't enter, not > 2 char
//         $.ajax({
//             type:"get",
//             url:'/search/' + keyword,
//             dataType: 'json',
//             success: function(data) {
//                 if(jQuery.isEmptyObject(data)) {
//                     btn_prev.addClass('no-display');
//                     btn_next.addClass('no-display');
//                     listing_table.innerHTML = "Tyvärr hittar vi ingen produkt som matchar din sökning.";
//                 }
//                 else {
//                     current_page = 1;
//                     objJson = data;
//                     $('#listingTable').empty();
//                     changePage(1);
//                 }
//             },
//             error: function(data){
//                 // console.log(data.error);
//             }
//         })
//     }

//     $('.prevPage').on('click', function (e) {
//         if (current_page > 1) {
//             current_page--;
//             changePage(current_page);
//         }
//     });

//     $('.nextPage').on('click', function (e) {
//         if (current_page < numPages()) {
//             current_page++;
//             changePage(current_page);
//         }
//     });

//     function changePage(page) {
//         if (page < 1) page = 1;
//         if (page > numPages()) page = numPages();

//         divider_forms.removeClass('no-display');
//         button_search_container.removeClass('no-display');

//         listing_table.innerHTML = "";
//         listing_table.innerHTML +=
//             '<thead>' +
//                 '<tr>' +
//                     '<th>Namn</th>' +
//                     '<th>Volym</th>' +
//                     '<th>Procent</th>' +
//                 '</tr>' +
//             '<thead>';

//         for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
//             listing_table.innerHTML +=
//                 '<tr>' +
//                     '<td>' + objJson[i].name + '</td>' +
//                     '<td>' + objJson[i].volume + 'cl</td>' +
//                     '<td>' + objJson[i].alcohol + '%</td>' +
//                     '<td><button type="button" id="' + i +'" class="addDrink">Ok</button></td>' +
//                 '</tr>';
//         }

//         if (page == 1) {
//             btn_prev.addClass('no-display');
//         } else {
//             btn_prev.removeClass('no-display');
//         }

//         if (page == numPages()) {
//             btn_next.addClass('no-display');
//         } else { 
//             btn_next.removeClass('no-display');
//         }
//     }

//     function numPages() {
//         return Math.ceil(objJson.length / records_per_page);
//     }

//     $('#listingTable').on('click', 'button.addDrink', function() {
//         search_result_selected.innerHTML +=
//             '<label class="label-selected">'+objJson[this.id].name + ' ' + objJson[this.id].volume + 'cl (' + objJson[this.id].alcohol + '%)</label>' +
//             '<br>' +
//             '<input type="text" class="form-control" name="drink['+ objJson[this.id].foreign_id +'][amount]" placeholder="cl">' +
//             '<input type="hidden" name="drink['+ objJson[this.id].foreign_id +'][alcohol]" value="' + objJson[this.id].alcohol + '">' +
//             '<br>';
//     });
// });
// var Ajax = (function () {

// 	var _privateMethod = function () {
// 		// private stuff
// 	};

// 	var publicMethod = function () {
// 		_privateMethod();
// 	};

// 	return {
// 		publicMethod: publicMethod
// 	};
// })();


// .then( response  =>  {
// 					if(Object.keys(response.data).length === 0) {
// 						this.button_search_container = false;
// 						this.listing_table = 'Tyvärr hittar vi ingen produkt som matchar din sökning.';
// 						this.search_result = [];
// 					}
// 					else {
// 						this.search_result = response.data;
// 					}
// 				}, (error)  =>  {
// 					console.log(error);
// 				});