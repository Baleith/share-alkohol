
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(function(){
    "use strict";

    $('#calculate-form').on('submit',function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        e.preventDefault(e);

        $.ajax({

            type:"POST",
            url:'/calculate',
            data:$(this).serialize(),
            dataType: 'json',
            success: function(data){
                console.log(data);
            },
            error: function(data){
                console.log(data.error);
            }
        })
    });

    $('#search').on('click', function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let keyword = document.getElementById('search-keyword').value;
        $.ajax({

            type:"get",
            url:'/search/' + keyword,
            dataType: 'json',
            success: function(data){
                objJson = data;
                changePage(1);
            },
            error: function(data){
                console.log(data.error);
            }
        })
    });

    var current_page = 1;
    var records_per_page = 9;
    var search_result_selected = document.getElementById("search-result-selected");

    var objJson = []; // Can be obtained from another source, such as your objJson variable

    $('.prevPage').on('click', function (e) {
        if (current_page > 1) {
            current_page--;
            changePage(current_page);
        }
    });

    $('.nextPage').on('click', function (e) {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page);
        }
    });

    function changePage(page) {
        var btn_next = document.getElementById("btn_next");
        var btn_prev = document.getElementById("btn_prev");
        var listing_table = document.getElementById("listingTable");

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages()) page = numPages();

        listing_table.innerHTML = "";

        for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
            listing_table.innerHTML += '<li><input type="checkbox" name="'+objJson[i].name+'" value="'+objJson[i].alcohol+'">' + objJson[i].name + "</li>";
        }

        if (page == 1) {
            btn_prev.style.visibility = "hidden";
        } else {
            btn_prev.style.visibility = "visible";
        }

        if (page == numPages()) {
            btn_next.style.visibility = "hidden";
        } else {
            btn_next.style.visibility = "visible";
        }
    }

    function numPages() {
        return Math.ceil(objJson.length / records_per_page);
    }
    $('#listingTable').on('change', 'input[type=checkbox]', function(e) {
        if (this.checked) {
            search_result_selected.innerHTML +=
                '<label>'+this.name+'</label>' +
                '<br>' +
                '<input type="text" name="added_by_search[]" placeholder="cl">' +
                '<input type="hidden" name="added_by_search[]" value="' + this.value + '">' +
                '<br>';
            console.log(this.name+' '+this.value+' '+this.checked);

        } else {
            // the checkbox is now no longer checked
        }


    });
    /*window.onload = function() {
        changePage(1);
    };*/
});
