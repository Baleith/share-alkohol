@extends('layout')

@section('content')

    <section class="section-form">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-7">
                <h1>Räkna ut din alkoholförbränning</h1>
                <div class="divider"></div>

                <component-a></component-a>
                {{-- @include('calculator_form') --}}

                <div class="alert alert-danger error-container no-display">
                    <ul id="error-list">

                    </ul>
                </div>

                <div class="result-container no-display">
                    {{--<p>Alla personer förbränner alkohol olika snabbt således kan vi endast ge ett ungefärligt svar.</p>--}}
                    <div class="alert alert-success">
                        Du har ungefär <span id="result-permille"></span> promille alkohol i blodet.
                        <br>

                    </div>
                </div>

            </div>

            <div class="col-sm-12 col-md-12 col-lg-5 alcohol-text">
                <h2>Hur lång tid tar förbränningen?</h2>
                <p>
                    En regel att gå efter är att levern förbränner ca 1,5-2 cl 40% sprit per timme.
                    De som väger mer förbränner mer än de som väger mindre, likaså förbränner män alkohol snabbare än kvinnor.
                    Exempel en man på 75 kg förbränner mer alkohol per timme än en kvinna på 75 kg.
                </p>
                <h2>Kan förbränningen påskyndas?</h2>
                <p>
                    Förbränningen går inte att påskynda. Att bada bastu eller att dricka kaffe skulle påverkar förbränningen är bara en myt.
                    Tänk på att även om all alkohol lämnat blodet är du inte helt återställd.
                </p>
                <h2>När kan jag köra bil?</h2>
                <p>
                    Vi kan inte mer än att på ett ungefär räkna ut hur lång tid det kommer ta innan du är helt nykter.
                    Två personer som väger lika mycket och har druckit samma mängd alkohol kommer troligtvis att ha olika promillehalter i blodet.
                </p>
                <br>
                <p>
                    Det har visats att personers körförmåga har varit nedsatts med upp till 20 procent efter de druckit alkohol även fast all alkohol lämnat blodet.
                    Det är alltid smartast att lämna bilen hemma dagen efter.
                </p>
                <br>
            </div>
        </div>
    </section>


@endsection