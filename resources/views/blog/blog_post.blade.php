@extends('layout')

@section('content')

    <div class="container">
        <section class="section-blog-post">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                    <div class="blog-post-container">

                        <p class="post-date">11/1-2018</p>
                        <h1>Hur påverkar alkohol dig</h1>
                        <img style="max-width: 100%;" src="{{asset('images/testblogg.jpg')}}" >

                        <h2>Hur fort går alkohol ur kroppen?</h2>
                        <p>
                            Att dricka starkt kaffe, bada bastu eller röra på sig gör alltså varken från eller till.
                            Om man väger mer än genomsnittet går det en aning snabbare och väger man mindre än genomsnittet går det lite långsammare.

                            Men bara för att alkoholen har lämnat kroppen är du inte är helt återställd.
                            Behöver man vara skärpt bör man vänta längre. Exempelvis har personers förmåga att köra bil visat sig vara nedsatt med hela 20 procent,
                            trots att testpersonerna varken haft alkohol kvar i kroppen eller känt sig bakfulla. Här finns lite mer rattfyllefakta....
                        </p>

                        <div id="share-buttons">
                            <p>Dela eller skicka till en vän</p>
                            <!-- Messenger -->
                            <a v-bind:href="'fb-messenger://share/?link='+currenturl+'&app_id=123456789'">
                                <img src="{{asset('images/messenger_icon.png')}}" alt="Email" />
                            </a>

                            <!-- SMS -->
                            <a href="'sms:070?body=Jag har hittat en galen lägenhet. Kolla här! '+currenturl">
                                <img src="{{asset('images/sms.jpg')}}" alt="Email" />
                            </a>

                            <!-- Email -->
                            <a href="'mailto:van@doman.se?subject=Lägenhet på styrbostad.se&body=Jag har hittat en galen lägenhet. Kolla här! '+currenturl">
                                <img src="{{asset('images/email.png')}}" alt="Email" />
                            </a>

                            <!-- Facebook -->
                            <a href="'https://www.facebook.com/sharer/sharer.php?u='+currenturl" target="_blank">
                                <img src="{{asset('images/facebook.png')}}" alt="Facebook" />
                            </a>

                            <!-- Google+ -->
                            <a href="https://plus.google.com/share?url=https://simplesharebuttons.com" target="_blank">
                                <img src="{{asset('images/google.png')}}" alt="Google" />
                            </a>

                            <!-- Reddit -->
                            <a href="http://reddit.com/submit?url=https://simplesharebuttons.com&amp;title=Simple Share Buttons" target="_blank">
                                <img src="{{asset('images/reddit.png')}}" alt="Reddit" />
                            </a>

                            <!-- Twitter -->
                            <a href="https://twitter.com/share?url=https://simplesharebuttons.com&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons" target="_blank">
                                <img src="{{asset('images/twitter.png')}}" alt="Twitter" />
                            </a>
                        </div> <!-- share-buttons -->
                    </div> <!-- blog-post-container -->
                </div>
            </div>
        </section>
    </div>

@endsection