@extends('layout')

@section('content')

        <section class="section-blog">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                    <div class="blog-post-container">
                        <h1>Hur påverkar alkohol dig</h1>
                        <p>11/1-2018</p>
                        <img style="max-width: 100%;" src="{{asset('images/testblogg.jpg')}}" >

                        <h2>Hur fort går alkohol ur kroppen?</h2>
                        <p>
                            Att dricka starkt kaffe, bada bastu eller röra på sig gör alltså varken från eller till.
                            Om man väger mer än genomsnittet går det en aning snabbare och väger man mindre än genomsnittet går det lite långsammare.

                            Men bara för att alkoholen har lämnat kroppen är du inte är helt återställd.
                            Behöver man vara skärpt bör man vänta längre. Exempelvis har personers förmåga att köra bil visat sig vara nedsatt med hela 20 procent,
                            trots att testpersonerna varken haft alkohol kvar i kroppen eller känt sig bakfulla. Här finns lite mer rattfyllefakta....
                        </p>
                        <div><a href="/get/post">Läs mer</a> </div>
                    </div> <!-- blog-post-container -->
                    <div class="blog-post-container">
                        <h1>Hur alkohol påverkar dig</h1>
                        <p>11/1-2018</p>
                        <img style="max-width: 100%;" src="{{asset('images/testblogg.jpg')}}" >

                        <h2>Hur fort går alkohol ur kroppen?</h2>
                        <p>
                            Att dricka starkt kaffe, bada bastu eller röra på sig gör alltså varken från eller till.
                            Om man väger mer än genomsnittet går det en aning snabbare och väger man mindre än genomsnittet går det lite långsammare.

                            Men bara för att alkoholen har lämnat kroppen är du inte är helt återställd.
                            Behöver man vara skärpt bör man vänta längre. Exempelvis har personers förmåga att köra bil visat sig vara nedsatt med hela 20 procent,
                            trots att testpersonerna varken haft alkohol kvar i kroppen eller känt sig bakfulla. Här finns lite mer rattfyllefakta....
                        </p>
                        <div><a href="/get/post">Läs mer</a> </div>
                    </div> <!-- blog-post-container -->
                    <div class="blog-post-container">
                        <h1>Hur alkohol påverkar dig</h1>
                        <p>11/1-2018</p>
                        <img style="max-width: 100%;" src="{{asset('images/testblogg.jpg')}}" >

                        <h2>Hur fort går alkohol ur kroppen?</h2>
                        <p>
                            Att dricka starkt kaffe, bada bastu eller röra på sig gör alltså varken från eller till.
                            Om man väger mer än genomsnittet går det en aning snabbare och väger man mindre än genomsnittet går det lite långsammare.

                            Men bara för att alkoholen har lämnat kroppen är du inte är helt återställd.
                            Behöver man vara skärpt bör man vänta längre. Exempelvis har personers förmåga att köra bil visat sig vara nedsatt med hela 20 procent,
                            trots att testpersonerna varken haft alkohol kvar i kroppen eller känt sig bakfulla. Här finns lite mer rattfyllefakta....
                        </p>
                        <div><a href="/get/post">Läs mer</a> </div>
                    </div> <!-- blog-post-container -->
                </div>
            </div>
        </section>

@endsection