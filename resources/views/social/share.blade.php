@extends('layout')

@section('content')

    <section class="section-share">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-7">
                <div id="share-buttons">
                    <p>Dela eller skicka till en vän</p>
                    <!-- Messenger -->
                    <a v-bind:href="'fb-messenger://share/?link='+currenturl+'&app_id=123456789'">
                        <img src="{{asset('images/messenger_icon.png')}}" alt="Email" />
                    </a>

                    <!-- SMS -->
                    <a href="'sms:070?body=Jag har hittat en galen lägenhet. Kolla här! '+currenturl">
                        <img src="{{asset('images/sms.jpg')}}" alt="Email" />
                    </a>

                    <!-- Email -->
                    <a href="'mailto:van@doman.se?subject=Lägenhet på styrbostad.se&body=Jag har hittat en galen lägenhet. Kolla här! '+currenturl">
                        <img src="{{asset('images/email.png')}}" alt="Email" />
                    </a>

                    <!-- Facebook -->
                    <a href="'https://www.facebook.com/sharer/sharer.php?u='+currenturl" target="_blank">
                        <img src="{{asset('images/facebook.png')}}" alt="Facebook" />
                    </a>

                    <!-- Google+ -->
                    <a href="https://plus.google.com/share?url=https://simplesharebuttons.com" target="_blank">
                        <img src="{{asset('images/google.png')}}" alt="Google" />
                    </a>

                    <!-- Reddit -->
                    <a href="http://reddit.com/submit?url=https://simplesharebuttons.com&amp;title=Simple Share Buttons" target="_blank">
                        <img src="{{asset('images/reddit.png')}}" alt="Reddit" />
                    </a>

                    <!-- Twitter -->
                    <a href="https://twitter.com/share?url=https://simplesharebuttons.com&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons" target="_blank">
                        <img src="{{asset('images/twitter.png')}}" alt="Twitter" />
                    </a>
                </div> <!-- share-buttons -->
            </div>
        </div>
    </section>

@endsection