<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Räkna ut din alkoholförbränning. Får ett tillförlitligt resultat om din nuvarande promille och tid kvar tills du är nykter.
    Fyll i ett enkelt formulär så sköter vi resten">

    <meta property="og:title" content="Räkna snabbt och enkelt ut din alkoholförbränning!" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.tillnyktrad.se/" />
    <meta property="og:image" content="{{asset('/images/loga.png')}}" />
    <meta property="og:description" content="Räkna ut din alkoholförbränning. Får ett tillförlitligt resultat om din nuvarande promille och tid kvar tills du är nykter.
    Fyll i ett enkelt formulär så sköter vi resten" />

    <title>Räkna snabbt och enkelt ut din alkoholförbränning!</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Icon -->
    <link rel="icon" href ="/favicon.ico" type="image/x-icon" /> <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="https://use.fontawesome.com/1657686afa.js"></script>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58459850-9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-58459850-9');
      gtag('config', 'UA-58459850-8');
    </script>


</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="{{URL::asset('/images/loga.png')}}" alt="Tillnyktrad loga">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li><a href="/">Tillnyktrad.se</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">Hem <span class="sr-only">(current)</span></a></li>
                    {{--<li><a href="/blog">Blogg</a></li>--}}
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container">

        @yield('content')

    </div>

    <footer>
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    {{--<div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="text-left">
                            <p>Tillnyktrad.se</p>
                        </div>
                    </div>--}}
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        {{--<div class="text-right">
                            <p>Kontakta oss på <a href="mailto:webmaster@example.com">Jon Doe</a>.<br></p>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="/js/app.js"></script>
</body>

</html>
