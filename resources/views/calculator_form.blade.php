<form method="post" action="/calculate" id="calculate-form">
    {{ csrf_field() }}
    <label for="male">Man</label>
    <input type="radio" name="gender[]" id="male" value="male">
    <label for="woman">Kvinna</label>
    <input type="radio" name="gender[]" id="woman" value="woman"><br>
    <label for="weight">Din vikt</label><br>
    <button type="button" class="btn-decrease btn-weight">-</button>
    <input type="text" name="weight" id="weight" value="75">
    <button type="button" class="btn-increase btn-weight">+</button>

    <div class="container-search">
        <label for="search-keyword">Sök dryck från Systembolagets sortiment</label>
        <div class="search-bar-container">
            <input type="text" name="keyword" id="search-keyword" placeholder="Dryck">
            <button type="button" id="search"><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
        <div class="search-results">
            <table id="listingTable">

                <!-- JS populated -->

            </table>
            <div class="button-search-container no-display">
                <button class="pageBtn prevPage" id="btn_prev" type="button"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
                <button class="pageBtn nextPage" id="btn_next" type="button"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>

    <div class="divider-forms no-display"></div>

   {{-- <h2>Fyll i nedan hur mycket alkohol du har druckit.</h2>--}}


    <div id="search-result-selected">

    </div>


    <label class="label-pre-fieldset" for="beer">Hur många/mycket öl drack du?</label>
    <fieldset>
        <input type="text" class="input-amount form-control" placeholder="Antal" id="beer">
        <select id="select-beer" name="beerType" class="form-control">
            <option value="33">33cl</option>
            <option value="50">50cl</option>
            <option value="1">cl</option>
        </select>
        <input type="hidden" name="drink[beer][amount]" class="form-control" value="" id="beer_hidden">
        <input type="text" class="input-percentage input-with-drop form-control" name="drink[beer][alcohol]" id="beerPercentage" value="5">
        <label for="beerPercentage">%</label><br>
    </fieldset>

    <label class="label-pre-fieldset" for="booze">Hur mycket starksprit drack du?</label>
    <fieldset>
        <input type="text" class="input-amount form-control" name="drink[booze][amount]" id="booze" placeholder="cl">
        <input type="text" class="input-percentage form-control" name="drink[booze][alcohol]" id="boozePercentage" value="40">
        <label for="boozePercentage">%</label><br>
    </fieldset>

    <label class="label-pre-fieldset" for="wine">Hur mycket vin drack du?</label>
    <fieldset>
        <input type="text" class="input-amount form-control" name="drink[wine][amount]" id="wine" placeholder="cl">
        <input type="text" class="input-percentage form-control" name="drink[wine][alcohol]" id="winePercentage" value="13">
        <label for="winePercentage">%</label><br>
    </fieldset>

    <label class="label-pre-fieldset" for="other">Drack du något annat?</label>
    <fieldset class="fieldset-small">
        <input type="text" class="input-amount form-control" name="drink[other][amount]" id="other" placeholder="cl">
        <input type="text" class="input-percentage form-control" name="drink[other][alcohol]" id="otherPercentage" value="22">
        <label for="otherPercentage">%</label><br>
    </fieldset>

    <label class="label-pre-fieldset" for="hours">Hur många timmar sedan drack du ditt första glas?</label>
    <fieldset class="fieldset-small">
        <input type="text" class="input-hours form-control" name="hours" id="hours" placeholder="0">
        <label class="label-hours" for="hours">timmar</label><br>
    </fieldset>

    <div class="button-container">
        <button type="reset" class="btn btn-default" id="reset">Återställ</button>

        <button class="btn btn-default" id="calculate">Räkna ut</button>
    </div>
</form>
